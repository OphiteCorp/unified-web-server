# Unified Web Server

This is a simple web server that allows you to select and access local files over the HTTP protocol.\
The application runs through the command line and has an HTTP interface and a configuration file.

- Files can normally be downloaded.
- Some files like text or images can be displayed in the browser.
- In the navigation header, you can click on the previous directories.
- All events are logged, including IP clients.
- The HTTP server is built on netty.io, so it should handle a higher load of access.
- Built specifically for anonymous listing of directories and files to download. Can be used, for example, for hidden services in Tor.
- The exit page does not contain any user or system information. Only the content of the page.

## Download
1) Go to the **Tags** section.
2) Find the latest version of the app.
3) Click the Download button -> Download artifacts -> package-release (it is a zip file, then unzip it).

## Requirements

- Java 11+ (JRE or JDK)

## How to use it

Download the application and run it with `java -jar unified-web-server.jar`. Without any other parameters.\
The configuration file and log file will be automatically generated. You can then stop the server and change the settings and run it again.

## Configuration

The application has only one configuration file, namely: `config.properties` which contains something like:

```properties
# The IP address the server will listen to. If 0.0.0.0 takes all available addresses
server.host = 0.0.0.0
server.port = 8080
# Root directory to be accessible from the page. It can be c:/ or /home/. I recommend using the absolute path
content.root = ./
# It applies to both files and directories
show.hidden.files = true
show.empty.dirs = true
# This information is next to the name of the file or directory
show.file.info = true
show.file.extension = true
# Remains only files from the current root directory. It is not possible to access the directories
show.directories = true
# Displays the top navigation bar
show.navbar = true
# Displays the bottom information bar
show.footer = true
# Creates the merge of all directories into the root directory. So it only shows files, but everything will include everything in the subdirectories
all.files.per.page = false
# Allows you to download and open files. Not applicable to directories
allow.file.downloads = true
# Regex pattern for directory filtering. If it is empty or .* It will allow everything
filter.file.pattern =
# Regex pattern for files filtering. If it is empty or .* It will allow everything
filter.dir.pattern =
```
**Specify some settings:**

I want to hide directories that start with a dot: `^[^.].*`\
I want to leave only directories that have a numeric name: `[0-9].*`\
It only displays directories called aaa or bbb: `aaa|bbb`\
Only displays properties files: `.*\\.properties`\
Retains only files that have a name and extension. And the file type must be at least 3 characters long: `.+[.](.+){3,}`\
Hides certain file types (exe, bat). It also hides files without type: `.*\\.(?!exe|bat).*$`

> All files or directories that are omitted are not visible and can not be accessed through the URL.

## Modding

The page layout is completely editable. Just open the `*.jar` file in some rar, zip, 7z etc and edit the `index.html` file. And save it again.\
You can change colors, layouts, or icons to file types. Of course only if you do not like the current look :)\
No java knowledge is needed for this.

## Screenshot

> The address was: `http://127.0.0.1:8080` for current directory `./`

![](media/screen.png)