package oc.mimic.uwebserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import oc.mimic.uwebserver.server.Initializer;
import oc.mimic.uwebserver.unified.ConfigLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Hlavní třída aplikace.
 *
 * @author mimic
 */
public final class Application {

  private static final Logger LOG = LoggerFactory.getLogger(Application.class);

  public static void main(String... args) throws IOException {
    var time = System.currentTimeMillis();
    LOG.info("Starting...");

    var config = ConfigLoader.load();
    var mainGroup = new NioEventLoopGroup(1);
    var workerGroup = new NioEventLoopGroup();

    try {
      var b = new ServerBootstrap();
      b.group(mainGroup, workerGroup);
      b.handler(new LoggingHandler(LogLevel.INFO));
      b.childHandler(new Initializer(config));
      b.channel(NioServerSocketChannel.class);

      var ch = b.bind(config.getServerHost(), config.getServerPort()).sync().channel();
      time = System.currentTimeMillis() - time;
      LOG.info("Started in: {}ms", time);
      LOG.info("The web server is available at: http://{}:{}", config.getServerHost(), config.getServerPort());
      ch.closeFuture().sync();

    } catch (Exception e) {
      LOG.error("An unexpected error has occurred and the server will be terminated", e);
    } finally {
      mainGroup.shutdownGracefully();
      workerGroup.shutdownGracefully();
    }
  }
}
