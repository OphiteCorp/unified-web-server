package oc.mimic.uwebserver.server;

import io.netty.channel.ChannelProgressiveFuture;
import io.netty.channel.ChannelProgressiveFutureListener;
import oc.mimic.uwebserver.unified.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.MessageFormat;
import java.time.Instant;

/**
 * Logika pro odchycení stavu stahování souboru.
 *
 * @author mimic
 */
final class DownloadProgressListener implements ChannelProgressiveFutureListener {

  private static final Logger LOG = LoggerFactory.getLogger(DownloadProgressListener.class);

  private final String relativePath;
  private final File file;
  private final Instant start;

  /**
   * Vytvoří novou instanci.
   *
   * @param rootPath Hlavní adresář, který je možné procházet.
   * @param file     Soubor, který se stahuje.
   */
  DownloadProgressListener(String rootPath, File file) {
    this.file = file;
    relativePath = Utils.getRelativePath(new File(rootPath), file);
    start = Instant.now();
  }

  @Override
  public void operationProgressed(ChannelProgressiveFuture future, long progress, long total) {
    var fileSizeStr = Utils.formatFileSize(total);
    var remoteAddr = Utils.getRemoteAddress(future.channel());

    if (total < 0) {
      LOG.debug("{} | Download: {} ({}) |-> {}", remoteAddr, relativePath, fileSizeStr, progress);
    } else {
      var progressStr = formattedProgress(total, progress);
      LOG.debug("{} | Download: {} ({}) |-> {}", remoteAddr, relativePath, fileSizeStr, progressStr);
    }
  }

  @Override
  public void operationComplete(ChannelProgressiveFuture future) {
    var fileSizeStr = Utils.formatFileSize(file.length());
    var remoteAddr = Utils.getRemoteAddress(future.channel());
    var time = Utils.formatTime(start, Instant.now());
    LOG.info("{} | Download: '{}' ({}) |-> Transfer complete in: {}", remoteAddr, relativePath, fileSizeStr, time);
  }

  private static String formattedProgress(long fileSize, long progress) {
    var currentDigits = Utils.getNumberOfDigits(progress);
    var totalDigits = Utils.getNumberOfDigits(fileSize);
    var percent = ((100. / fileSize) * progress) / 100.;
    var min = Math.min(totalDigits, currentDigits);
    var progressStr = " ".repeat(totalDigits - min) + progress;

    return MessageFormat.format("{0} / {1} >> {2,number,#.###%}", progressStr, String.valueOf(fileSize), percent);
  }
}
