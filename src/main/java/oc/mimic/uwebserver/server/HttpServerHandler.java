package oc.mimic.uwebserver.server;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.DefaultFileRegion;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import oc.mimic.uwebserver.unified.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

/**
 * Hlavní logika pro HTTP server.
 *
 * @author mimic
 */
abstract class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

  private static final int HTTP_CACHE_SECONDS = 60;
  private static final SimpleDateFormat SDF;

  private final Logger log;
  private final File rootDir;

  static {
    SDF = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
    SDF.setTimeZone(TimeZone.getTimeZone("GMT"));
  }

  /**
   * Vytvoří novou instanci HTTP handleru.
   *
   * @param rootPath Hlavní adresář, který je možné procházet.
   */
  HttpServerHandler(String rootPath) {
    this.rootDir = new File(rootPath);
    log = LoggerFactory.getLogger(getClass());
    log.debug("Root path: '{}'", rootDir.getAbsolutePath());
  }

  @Override
  public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
    var startTime = Instant.now();
    final var remoteAddress = Utils.getRemoteAddress(ctx.channel());
    final var reqUri = request.uri();

    log.debug("{} | '{}'", remoteAddress, reqUri);

    if (!request.decoderResult().isSuccess()) {
      log.warn("{} | Invalid request: '{}'", remoteAddress, reqUri);
      onAnotherStatus(ctx, request, HttpResponseStatus.BAD_REQUEST, startTime);
      return;
    }

    // podporujeme pouze metodu GET
    if (request.method() != HttpMethod.GET) {
      log.warn("{} | The method {} is noot allowed: '{}'", remoteAddress, request.method().name(), reqUri);
      onAnotherStatus(ctx, request, HttpResponseStatus.METHOD_NOT_ALLOWED, startTime);
      return;
    }

    // kořenový adresář musí být validní a existovat
    if (!rootDir.isDirectory() || !rootDir.exists()) {
      log.warn("{} | The root directory does not exist: '{}'", remoteAddress, rootDir.getPath());
      onAnotherStatus(ctx, request, HttpResponseStatus.GONE, startTime);
      return;
    }

    final var path = sanitizeUri(reqUri); // základní validace souboru nebo adresáře z URL
    if (path == null) {
      log.warn("{} | Invalid URL or a non-existent file: '{}'", remoteAddress, reqUri);
      onAnotherStatus(ctx, request, HttpResponseStatus.FORBIDDEN, startTime);
      return;
    }

    final var file = new File(path);
    final var relativePath = Utils.getRelativePath(rootDir, file);
    if (!file.canRead()) {
      log.warn("{} | The file can not be read: '{}'", remoteAddress, relativePath);
      onAnotherStatus(ctx, request, HttpResponseStatus.NOT_FOUND, startTime);
      return;
    } else if (!isFilePermitted(file)) {
      log.warn("{} | The file was rejected by custom validation: '{}'", remoteAddress, relativePath);
      onAnotherStatus(ctx, request, HttpResponseStatus.NOT_FOUND, startTime);
      return;
    }

    if (file.isDirectory()) {
      var fixedUri = Utils.removeDupeSlashes(reqUri);
      // v případě, že URI obsahuje adresář a zároveň neobsahuje duplicitní lomítka, tak přejde do adresáře
      if (reqUri.endsWith("/") && fixedUri.length() == reqUri.length()) {
        log.info("{} | Open: '{}'", remoteAddress, relativePath);
        onSendDirectoryListing(ctx, file, reqUri, HttpResponseStatus.OK, startTime);
      } else {
        sendRedirect(ctx, reqUri + '/');
      }
      return;
    }

    // pro jistotu znovu zkontrolujeme, ze se se jedná o soubor, protože stahovat adresáře nechceme
    if (!file.isFile()) {
      log.warn("{} | The download directory is not allowed: '{}'", remoteAddress, relativePath);
      onAnotherStatus(ctx, request, HttpResponseStatus.FORBIDDEN, startTime);
      return;
    }
    downloadFile(ctx, request, file, relativePath, remoteAddress, startTime);
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    log.error(Utils.getRemoteAddress(ctx.channel()) + " | Unexpected client error: " + cause.getMessage(), cause);
    if (ctx.channel().isActive()) {
      onAnotherStatus(ctx, null, HttpResponseStatus.INTERNAL_SERVER_ERROR, Instant.now());
    }
  }

  /**
   * Získá instanci loggeru.
   *
   * @return Logger.
   */
  final Logger getLog() {
    return log;
  }

  /**
   * Nějaká chyba. Logika vrátila jiný stav než 200 (OK).
   *
   * @param ctx       Kontext klienta.
   * @param request   Požadavek.
   * @param status    Stav, který nastal.
   * @param startTime Čas zahájení zpracovávání požadavku.
   */
  protected abstract void onAnotherStatus(ChannelHandlerContext ctx, FullHttpRequest request, HttpResponseStatus status,
          Instant startTime);

  /**
   * Vypsání adresáře. Jde o generování HTML stránky.
   *
   * @param ctx        Kontext klienta.
   * @param dir        Adresář, do kterého klient přistoupil.
   * @param requestUri URI požadavku.
   * @param status     Status požadavku.
   * @param startTime  Čas zahájení zpracovávání požadavku.
   */
  protected abstract void onSendDirectoryListing(ChannelHandlerContext ctx, File dir, String requestUri,
          HttpResponseStatus status, Instant startTime);

  /**
   * Použije vlastní (dodatečnou) validaci souboru.
   *
   * @param file Vstupní soubor nebo adresář.
   *
   * @return True, pokud je soubor v pořádku a může se dále zpracovat.
   */
  protected boolean isFilePermitted(File file) {
    return (file != null && file.exists() && file.canRead());
  }

  /**
   * Vyhodnotí, zda se jedná o root adresář.
   *
   * @param file Vstupní soubor k vyhodnocení.
   *
   * @return True, pokud jde o root adresář.
   */
  final boolean isRootFile(File file) {
    var path = Utils.removeLastSlash((file != null) ? file.getPath() : null);
    return rootDir.getPath().equals(path);
  }

  /**
   * Přesměruje uživatele na jinou URL.
   *
   * @param ctx    Kontext přihlášeného uživatele.
   * @param newUri Nová URL.
   */
  void sendRedirect(ChannelHandlerContext ctx, String newUri) {
    var uri = Utils.removeDupeSlashes(newUri);
    var remoteAddress = Utils.getRemoteAddress(ctx.channel());
    log.debug("{} | Redirect to: '{}'", remoteAddress, uri);

    var response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FOUND);
    response.headers().set(HttpHeaderNames.LOCATION, uri);
    ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
  }

  void downloadFile(ChannelHandlerContext ctx, FullHttpRequest request, File file, String relativePath,
          String remoteAddress, Instant startTime) throws IOException, ParseException {

    log.debug("{} | Preparing to download the file: '{}'", remoteAddress, relativePath);
    var ifModifiedSince = request.headers().get(HttpHeaderNames.IF_MODIFIED_SINCE);
    if (ifModifiedSince != null && !ifModifiedSince.isEmpty()) {
      var ifModifiedSinceDate = SDF.parse(ifModifiedSince);
      var ifModifiedSinceDateSeconds = ifModifiedSinceDate.getTime() / 1000;
      var fileLastModifiedSeconds = file.lastModified() / 1000;

      if (ifModifiedSinceDateSeconds == fileLastModifiedSeconds) {
        sendNotModified(ctx);
        return;
      }
    }
    RandomAccessFile raf;
    try {
      raf = new RandomAccessFile(file, "r");
    } catch (FileNotFoundException ignore) {
      log.error("{} | The file no longer exists: '{}'", remoteAddress, relativePath);
      onAnotherStatus(ctx, request, HttpResponseStatus.NOT_FOUND, startTime);
      return;
    }
    var fileLength = raf.length();
    var response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
    HttpUtil.setContentLength(response, fileLength);
    setContentTypeHeader(response, file);
    setDateAndCacheHeaders(response, file);

    if (HttpUtil.isKeepAlive(request)) {
      response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
    }
    ctx.write(response);

    var sendFileFuture = ctx.write(new DefaultFileRegion(raf.getChannel(), 0, fileLength), ctx.newProgressivePromise());
    var lastContentFuture = ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
    sendFileFuture.addListener(new DownloadProgressListener(rootDir.getPath(), file));

    if (!HttpUtil.isKeepAlive(request)) {
      lastContentFuture.addListener(ChannelFutureListener.CLOSE);
    }
  }

  private String sanitizeUri(String uri) {
    try {
      // upravíme URI pro lepší validaci
      uri = URLDecoder.decode(uri, StandardCharsets.UTF_8);
      uri = Utils.replaceSlashes(uri);
      uri = Utils.removeDupeSlashes(uri);
    } catch (Exception e) {
      return null;
    }
    // URI musí vždy začínat lomítkem
    if (uri.isEmpty() || uri.charAt(0) != '/') {
      return null;
    }
    uri = uri.replace('/', File.separatorChar);
    try {
      var path = Paths.get(rootDir.getPath(), uri);
      // zakáže přístup mimo root adresář (správně by to měl řešit samotný request)
      if (!path.toFile().getAbsolutePath().startsWith(rootDir.getAbsolutePath())) {
        return null;
      }
      if (path.toFile().exists()) {
        return path.toString();
      }
    } catch (Exception e) {
      // nic, zaloguje se později
    }
    return null;
  }

  private static void sendNotModified(ChannelHandlerContext ctx) {
    var response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_MODIFIED);
    setDateHeader(response);
    ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
  }

  private static Calendar setDateHeader(HttpResponse response) {
    var time = new GregorianCalendar();
    response.headers().set(HttpHeaderNames.DATE, SDF.format(time.getTime()));
    return time;
  }

  private static void setDateAndCacheHeaders(HttpResponse response, File fileToCache) {
    var time = setDateHeader(response);
    time.add(Calendar.SECOND, HTTP_CACHE_SECONDS);

    response.headers().set(HttpHeaderNames.EXPIRES, SDF.format(time.getTime()));
    response.headers().set(HttpHeaderNames.CACHE_CONTROL, "private, max-age=" + HTTP_CACHE_SECONDS);
    response.headers().set(HttpHeaderNames.LAST_MODIFIED, SDF.format(new Date(fileToCache.lastModified())));
  }

  private static void setContentTypeHeader(HttpResponse response, File file) {
    var mimeTypesMap = new MimetypesFileTypeMap();
    response.headers().set(HttpHeaderNames.CONTENT_TYPE, mimeTypesMap.getContentType(file.getPath()));
    // response.headers().set(HttpHeaderNames.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
  }
}
