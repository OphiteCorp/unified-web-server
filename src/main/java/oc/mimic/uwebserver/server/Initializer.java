package oc.mimic.uwebserver.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;
import oc.mimic.uwebserver.unified.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initializer web serveru.
 *
 * @author mimic
 */
public final class Initializer extends ChannelInitializer<SocketChannel> {

  private static final Logger LOG = LoggerFactory.getLogger(Initializer.class);

  private final Config config;

  /**
   * Vytvoří novou instanci.
   *
   * @param config Konfigurace aplikace.
   */
  public Initializer(Config config) {
    this.config = config;
    LOG.info("The root directory has been set to: '{}'", config.getContentRoot());
  }

  @Override
  public void initChannel(SocketChannel ch) {
    var pipeline = ch.pipeline();
    pipeline.addLast(new HttpServerCodec());
    pipeline.addLast(new HttpObjectAggregator(65536));
    pipeline.addLast(new ChunkedWriteHandler());
    pipeline.addLast(new UnifiedServerHandler(config));
  }
}
