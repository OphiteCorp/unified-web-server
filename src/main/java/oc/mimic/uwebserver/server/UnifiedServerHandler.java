package oc.mimic.uwebserver.server;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import oc.mimic.uwebserver.Application;
import oc.mimic.uwebserver.unified.Config;
import oc.mimic.uwebserver.unified.FileIconResolver;
import oc.mimic.uwebserver.unified.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author mimic
 */
final class UnifiedServerHandler extends HttpServerHandler {

  private static final String ROOT_NAME = "root";
  private static final String HTML_TEMPLATE;

  private final Config config;
  private Pattern dirPattern;
  private Pattern filePattern;

  static {
    var htmlTemplate = Application.class.getClassLoader().getResourceAsStream("index.html");
    HTML_TEMPLATE = new BufferedReader(new InputStreamReader(htmlTemplate)).lines().collect(Collectors.joining("\n"))
            .replaceAll("\\n", "");
  }

  /**
   * Vytvoří novou instanci.
   *
   * @param config Konfigurace aplikace.
   */
  UnifiedServerHandler(Config config) {
    super(config.getContentRoot());
    this.config = config;
    getLog().trace("Config: {}", config);
    getLog().trace("HTML template:\n{}", HTML_TEMPLATE);

    try {
      if (!config.getFilterDirPattern().isBlank()) {
        dirPattern = Pattern.compile(config.getFilterDirPattern());
      }
    } catch (Exception e) {
      getLog().error("Invalid filter dir pattern: " + config.getFilterDirPattern(), e);
    }
    try {
      if (!config.getFilterFilePattern().isBlank()) {
        filePattern = Pattern.compile(config.getFilterFilePattern());
      }
    } catch (Exception e) {
      getLog().error("Invalid filter file pattern: " + config.getFilterFilePattern(), e);
    }
  }

  @Override
  protected void onAnotherStatus(ChannelHandlerContext ctx, FullHttpRequest request, HttpResponseStatus status,
          Instant startTime) {

    if (status == HttpResponseStatus.NOT_FOUND || status == HttpResponseStatus.FORBIDDEN) {
      sendRedirect(ctx, "/");
    } else {
      onSendDirectoryListing(ctx, null, null, status, startTime);
    }
  }

  @Override
  protected boolean isFilePermitted(File file) {
    var permitted = isFileValid(file);
    if (permitted) {
      // skryje prázdné adresáře
      var info = getDirInfo(file);
      if (!config.isShowEmptyDirs() && file.isDirectory() && info.isEmpty()) {
        return false;
      }
    }
    return permitted;
  }

  @Override
  protected void onSendDirectoryListing(ChannelHandlerContext ctx, File dir, String dirPath, HttpResponseStatus status,
          Instant startTime) {

    // za všech okolností vrátíme stav OK
    var response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
    updateHeaders(response);

    var title = "[Unified] ";
    var header = "";
    var footer = "";
    var body = new StringBuilder();

    if (status == HttpResponseStatus.OK) {
      var decodedPath = URLDecoder.decode(dirPath, StandardCharsets.UTF_8);
      decodedPath = Utils.replaceSlashes(decodedPath);
      decodedPath = Utils.removeDupeSlashes(decodedPath);

      var info = getDirInfo(dir);
      body.append("<ul>");

      // vytvoří tlačítko zpět ".."
      if (!"/".equals(decodedPath)) {
        body.append(createHtmlLi(null));
      }
      for (var d : info.dirs) {
        insertHtmlFile(d, body);
      }
      for (var f : info.files) {
        insertHtmlFile(f, body);
      }
      body.append("</ul>");

      title += decodedPath;
      header += prepareHeader(decodedPath);
      footer += prepareFooter(info.dirs.size(), info.files.size(), Utils.formatFileSize(info.totalFilesSize),
              startTime);

    } else {
      title += '/';
      header += prepareHeader("/");
      footer += prepareFooter(0, 0, Utils.formatFileSize(0), startTime);

      // kořenový adresář nebyl nalezen
      if (status == HttpResponseStatus.GONE) {
        body.append("<h4>").append("The directory was stolen ¯\\_(ツ)_/¯").append("</h4>");
      } else if (status == HttpResponseStatus.LOCKED) {
        body.append("<h4>").append("Do it again and you will be expelled from our country (∩｀-´)⊃━☆ﾟ.*･｡ﾟ")
                .append("</h4>");
      } else {
        body.append("<h4 class=\"error\">").append("Fuck, something fucked on my great framework (╯°□°）╯︵ ┻━┻")
                .append("</h4>");
      }
    }
    var html = prepareHtml(title, header, body.toString(), footer);
    var buffer = Unpooled.copiedBuffer(html, CharsetUtil.UTF_8);

    response.content().writeBytes(buffer);
    buffer.release();
    ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
  }

  @Override
  void downloadFile(ChannelHandlerContext ctx, FullHttpRequest request, File file, String relativePath,
          String remoteAddress, Instant startTime) throws IOException, ParseException {

    if (config.isAllowFileDownloads()) {
      super.downloadFile(ctx, request, file, relativePath, remoteAddress, startTime);
    } else {
      onSendDirectoryListing(ctx, file, relativePath, HttpResponseStatus.LOCKED, startTime);
    }
  }

  private DirInfo getDirInfo(File dir) {
    var info = getDirInfo(dir, config.isAllFilesPerPage());

    if (config.isAllFilesPerPage()) {
      info.files.sort(Comparator.comparing(File::getName));
    }
    return info;
  }

  private DirInfo getDirInfo(File dir, boolean recursive) {
    var info = new DirInfo();

    if (!dir.isDirectory()) {
      return info;
    }
    dir.listFiles((d, n) -> {
      var file = Paths.get(d.getPath(), n).toFile();

      if (!isFileValid(file)) {
        return false;
      }
      if (file.isDirectory()) {
        if (recursive) {
          var subInfo = getDirInfo(file);
          info.dirs.addAll(subInfo.dirs);
          info.files.addAll(subInfo.files);
          info.totalFilesSize += subInfo.totalFilesSize;
        } else {
          info.dirs.add(file);
        }
      } else {
        info.files.add(file);
        info.totalFilesSize += file.length();
      }
      return true;
    });
    return info;
  }

  private boolean isFileValid(File file) {
    // základní validace
    if (!super.isFilePermitted(file)) {
      return false;
    }
    // skryje všechny adresáře pokud je to potřeba
    if (!config.isShowDirectories() && file.isDirectory() && !isRootFile(file)) {
      return false;
    }
    var rootFile = isRootFile(file);
    // mají se zobrazit i skryté soubory a adresáře? (root je vždy označen jako skrytý, takže ho přeskočíme)
    if (!config.isShowHiddenFiles() && !rootFile && file.isHidden()) {
      return false;
    }
    if (!rootFile) {
      Matcher matches = null;
      if (file.isDirectory() && dirPattern != null) {
        matches = dirPattern.matcher(file.getName());
      } else if (file.isFile() && filePattern != null) {
        matches = filePattern.matcher(file.getName());
      }
      return (matches == null || matches.find());
    }
    return true;
  }

  private static String buildNavigation(final String decodedPath) {
    final var rootAlias = ROOT_NAME;
    var path = decodedPath;
    var map = new HashMap<String, String>();
    var sb = new StringBuilder();
    var i = -1;

    // prvně sestavíme pomocnou mapu, která obsahuje jméno adresáře a relativní URI na adresář od rootu
    if (!path.equals("/")) {
      if (path.endsWith("/")) {
        path = path.substring(0, path.length() - 1);
      }
      while ((i = path.lastIndexOf('/')) != -1) {
        var name = path.substring(i + 1);
        path = path.substring(0, i);
        var url = path + '/' + name;
        map.put(name, url);
      }
    }
    map.put(rootAlias, "/");
    path = decodedPath;

    // upravímé root adresář
    var parts = path.split("/");
    if (parts.length == 0) {
      parts = new String[]{ rootAlias };
    } else {
      parts[0] = rootAlias;
    }
    // poté vygenerujeme odkazy pro každý adresář v navigaci
    for (i = 0; i < parts.length; i++) {
      sb.append(String.format("<a href=\"%s\">%s</a>", map.get(parts[i]), parts[i]));

      if (i < parts.length - 1) {
        sb.append("/");
      }
    }
    return sb.toString();
  }

  private void insertHtmlFile(File file, StringBuilder sb) {
    if (!isFilePermitted(file)) {
      return;
    }
    sb.append(createHtmlLi(file));
  }

  private String createHtmlLi(File file) {
    var backButton = (file == null);
    var fileTypeCssClass = (backButton ? "back" : (file.isDirectory() ? "dir" : "file"));
    var title = !backButton ? (file.isDirectory() ? "Directory" : "File") : "Go to the previous directory";
    var format = "";
    format += "<li class=\"" + fileTypeCssClass + "\">";
    format += "<a href=\"%s\">";
    format += "<div class=\"item\">";
    format += "<div class=\"icon " + fileTypeCssClass + "%s\" title=\"" + title + "\"></div>";
    format += "<div class=\"link\">%s</div>";
    format += "</div>";
    format += "</a>";
    format += "</li>";

    if (backButton) {
      return String.format(format, "../", "", "..");
    } else {
      var relativePath = file.getName();
      if (config.isAllFilesPerPage()) {
        relativePath = Utils.getRelativePath(new File(config.getContentRoot()), file).substring(1);
      }
      var name = config.isShowFileExtension() ? file.getName() : Utils.removeFileExtension(file.getName());
      var extIcon = file.isFile() ? " " + FileIconResolver.resolve(file) : "";
      var fileUrl = URLEncoder.encode(relativePath, StandardCharsets.UTF_8);

      if (config.isShowFileInfo()) {
        var info = getDirInfo(file);
        var hiddenSpanFlag = file.isHidden() ? "<span class=\"hidden\">hidden</span>" : "";
        var fileSizeSpanFlag =
                file.isFile() ? String.format("<span class=\"size\">%s</span>", Utils.formatFileSize(file.length()))
                              : "";
        var sumSpanFlag = file.isDirectory() ? String
                .format("<span class=\"sum\">%s/%s</span>", info.dirs.size(), info.files.size()) : "";

        var fileInfo = Utils.join(" | ", fileSizeSpanFlag, sumSpanFlag, hiddenSpanFlag);
        name += String.format("<span class=\"info\">(%s)</span>", fileInfo);
      }
      var li = String.format(format, fileUrl, extIcon, name);

      if (file.isFile() && !config.isAllowFileDownloads()) {
        li = li.replaceFirst("<a.href=.+?>(.+)</a>", "$1");
      }
      return li;
    }
  }

  private String prepareHeader(String decodedPath) {
    var sb = new StringBuilder();
    if (config.isShowNavBar()) {
      sb.append("<header>");
      sb.append("<h3>/").append(buildNavigation(decodedPath)).append("</h3>");
      sb.append("</header>");
    }
    return sb.toString();
  }

  private String prepareFooter(int totalDirs, int totalFiles, String totalFileSize, Instant startTime) {
    var sb = new StringBuilder();
    if (config.isShowFooter()) {
      sb.append("<footer>");
      sb.append("Directories: <span class=\"value\">").append(totalDirs).append("</span> | ");
      sb.append("Files: <span class=\"value\">").append(totalFiles).append("</span> | ");
      sb.append("Total file size in the directory: <span class=\"value\">").append(totalFileSize).append("</span> | ");
      sb.append("Processing time: <span class=\"value\">").append(Utils.formatTime(startTime, Instant.now()))
              .append("</span>");
      sb.append("</footer>");
    }
    return sb.toString();
  }

  private String prepareHtml(String title, String header, String body, String footer) {
    var contentClasses = Utils
            .join(" ", config.isShowNavBar() ? "show-navbar" : "", config.isShowFooter() ? "show-footer" : "");
    var html = HTML_TEMPLATE;
    html = html.replaceAll(Pattern.quote("{TITLE}"), Matcher.quoteReplacement(title));
    html = html.replaceAll(Pattern.quote("{HEADER}"), Matcher.quoteReplacement(header));
    html = html.replaceAll(Pattern.quote("{CONTENT_CLASSES}"), Matcher.quoteReplacement(contentClasses));
    html = html.replaceAll(Pattern.quote("{BODY}"), Matcher.quoteReplacement(body));
    html = html.replaceAll(Pattern.quote("{FOOTER}"), Matcher.quoteReplacement(footer));
    return html;
  }

  private static void updateHeaders(FullHttpResponse response) {
    response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html; charset=UTF-8");
  }

  private static final class DirInfo {

    private final List<File> dirs = new ArrayList<>();
    private final List<File> files = new ArrayList<>();
    private long totalFilesSize;

    private int getSumFiles() {
      return (dirs.size() + files.size());
    }

    private boolean isEmpty() {
      return (getSumFiles() == 0);
    }
  }
}
