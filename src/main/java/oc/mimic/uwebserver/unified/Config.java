package oc.mimic.uwebserver.unified;

import java.util.StringJoiner;

/**
 * Obsahuje konfiguraci aplikace.
 *
 * @author mimic
 */
public final class Config {

  private String serverHost;
  private int serverPort;
  private String contentRoot;
  private boolean showHiddenFiles;
  private boolean showEmptyDirs;
  private boolean showFileInfo;
  private boolean showFileExtension;
  private boolean showDirectories;
  private boolean showNavBar;
  private boolean showFooter;
  private boolean allFilesPerPage;
  private boolean allowFileDownloads;
  private String filterDirPattern;
  private String filterFilePattern;

  public String getContentRoot() {
    return contentRoot;
  }

  void setContentRoot(String contentRoot) {
    this.contentRoot = contentRoot;
  }

  public boolean isShowHiddenFiles() {
    return showHiddenFiles;
  }

  void setShowHiddenFiles(boolean showHiddenFiles) {
    this.showHiddenFiles = showHiddenFiles;
  }

  public boolean isShowEmptyDirs() {
    return showEmptyDirs;
  }

  void setShowEmptyDirs(boolean showEmptyDirs) {
    this.showEmptyDirs = showEmptyDirs;
  }

  public boolean isShowFileInfo() {
    return showFileInfo;
  }

  void setShowFileInfo(boolean showFileInfo) {
    this.showFileInfo = showFileInfo;
  }

  public boolean isShowFooter() {
    return showFooter;
  }

  void setShowFooter(boolean showFooter) {
    this.showFooter = showFooter;
  }

  public boolean isShowNavBar() {
    return showNavBar;
  }

  void setShowNavBar(boolean showNavBar) {
    this.showNavBar = showNavBar;
  }

  public boolean isShowDirectories() {
    return showDirectories;
  }

  void setShowDirectories(boolean showDirectories) {
    this.showDirectories = showDirectories;
  }

  public boolean isAllFilesPerPage() {
    return allFilesPerPage;
  }

  void setAllFilesPerPage(boolean allFilesPerPage) {
    this.allFilesPerPage = allFilesPerPage;
  }

  public boolean isShowFileExtension() {
    return showFileExtension;
  }

  void setShowFileExtension(boolean showFileExtension) {
    this.showFileExtension = showFileExtension;
  }

  public String getFilterDirPattern() {
    return filterDirPattern;
  }

  void setFilterDirPattern(String filterDirPattern) {
    this.filterDirPattern = filterDirPattern;
  }

  public String getFilterFilePattern() {
    return filterFilePattern;
  }

  void setFilterFilePattern(String filterFilePattern) {
    this.filterFilePattern = filterFilePattern;
  }

  public boolean isAllowFileDownloads() {
    return allowFileDownloads;
  }

  void setAllowFileDownloads(boolean allowFileDownloads) {
    this.allowFileDownloads = allowFileDownloads;
  }

  public int getServerPort() {
    return serverPort;
  }

  void setServerPort(int serverPort) {
    this.serverPort = serverPort;
  }

  public String getServerHost() {
    return serverHost;
  }

  void setServerHost(String serverHost) {
    this.serverHost = serverHost;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Config.class.getSimpleName() + "[", "]").add("serverHost='" + serverHost + "'")
            .add("serverPort=" + serverPort).add("contentRoot='" + contentRoot + "'")
            .add("showHiddenFiles=" + showHiddenFiles).add("showEmptyDirs=" + showEmptyDirs)
            .add("showFileInfo=" + showFileInfo).add("showFileExtension=" + showFileExtension)
            .add("showDirectories=" + showDirectories).add("showNavBar=" + showNavBar).add("showFooter=" + showFooter)
            .add("allFilesPerPage=" + allFilesPerPage).add("allowFileDownloads=" + allowFileDownloads)
            .add("filterDirPattern='" + filterDirPattern + "'").add("filterFilePattern='" + filterFilePattern + "'")
            .toString();
  }
}
