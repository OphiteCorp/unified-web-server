package oc.mimic.uwebserver.unified;

import oc.mimic.uwebserver.Application;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Logika pro načtení a správu konfigurace aplikace.
 *
 * @author mimic
 */
public final class ConfigLoader {

  private static final String CONFIG_FILE = "config.properties";

  /**
   * Načte konfigurační soubor. Pokud nebude existovat, tak ho vytvoří.
   *
   * @return Nová instance konfigurace.
   */
  public static Config load() throws IOException {
    var file = new File(CONFIG_FILE);
    var props = new Properties();

    if (!file.exists()) {
      try (var stream = Application.class.getClassLoader().getResourceAsStream(CONFIG_FILE)) {
        Files.copy(stream, Paths.get(CONFIG_FILE));
      }
    }
    if (file.exists()) {
      try (var stream = new FileInputStream(file)) {
        props.load(stream);
      }
    } else {
      throw new RuntimeException("An unexpected error occurred while loading the configuration");
    }
    var contentRoot = Utils.removeDupeSlashes(props.get("content.root").toString());
    contentRoot = Utils.addLastSlash(contentRoot);

    var config = new Config();
    config.setServerHost(props.get("server.host").toString());
    config.setServerPort(Integer.parseInt(props.get("server.port").toString()));
    config.setContentRoot(contentRoot);
    config.setShowHiddenFiles(Boolean.parseBoolean(props.get("show.hidden.files").toString()));
    config.setShowEmptyDirs(Boolean.parseBoolean(props.get("show.empty.dirs").toString()));
    config.setShowFileInfo(Boolean.parseBoolean(props.get("show.file.info").toString()));
    config.setShowFileExtension(Boolean.parseBoolean(props.get("show.file.extension").toString()));
    config.setShowDirectories(Boolean.parseBoolean(props.get("show.directories").toString()));
    config.setShowNavBar(Boolean.parseBoolean(props.get("show.navbar").toString()));
    config.setShowFooter(Boolean.parseBoolean(props.get("show.footer").toString()));
    config.setAllFilesPerPage(Boolean.parseBoolean(props.get("all.files.per.page").toString()));
    config.setAllowFileDownloads(Boolean.parseBoolean(props.get("allow.file.downloads").toString()));
    config.setFilterDirPattern(props.get("filter.dir.pattern").toString());
    config.setFilterFilePattern(props.get("filter.file.pattern").toString());
    return config;
  }
}
