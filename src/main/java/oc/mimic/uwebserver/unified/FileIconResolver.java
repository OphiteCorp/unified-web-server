package oc.mimic.uwebserver.unified;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Vyhodnocuje ikony souborů.
 *
 * @author mimic
 */
public final class FileIconResolver {

  private static final Logger LOG = LoggerFactory.getLogger(FileIconResolver.class);

  private static final Map<String, List<String>> MAP;

  static {
    MAP = new HashMap<>();
    // seznam kolekcí, které sjednocují ikonu souboru
    MAP.put("archive", Arrays.asList( //
            "7z", "arj", "deb", "pkg", "rar", "rpm", "tar.gz", "z", "zip", "gz"));

    MAP.put("video", Arrays.asList( //
            "3g2", "3gp", "avi", "flv", "h264", "m4v", "mkv", "mov", "mp4", "mpg", "mpeg", "rm", "swf", "vob", "wmv"));

    MAP.put("audio", Arrays.asList( //
            "aif", "cda", "mid", "midi", "mp3", "mpa", "ogg", "wav", "wma", "wpl", "m4a"));

    MAP.put("font", Arrays.asList( //
            "fnt", "fon", "otf", "ttf", "ttc"));

    MAP.put("image", Arrays.asList( //
            "ai", "bmp", "gif", "ico", "jpeg", "jpg", "png", "ps", "psd", "svg", "tif", "tiff", "tga"));

    MAP.put("document", Arrays.asList( //
            "txt", "doc", "docx", "odt", "pdf", "rtf", "tex", "wps", "wks", "wpd", "log", "nfo", "info", "manifest",
            "csv"));

    MAP.put("documentation", Arrays.asList( //
            "md"));

    MAP.put("config", Arrays.asList( //
            "ini", "conf", "config", "properties", "yml", "yaml", "cfg", "prefs", "gradle"));

    MAP.put("executable", Arrays.asList( //
            "exe", "bat", "cmd"));

    MAP.put("source", Arrays.asList( //
            "c", "h", "cpp", "java", "php", "js", "css", "cs", "py", "pl", "vb", "swift", "sh", "class", "asp", "aspx",
            "html", "htm", "cgi", "jsp", "jsf", "rss", "xhtml", "sql", "xml", "vdf", "script", "lua", "ui", "pro",
            "sln", "vbs"));

    MAP.put("data", Arrays.asList( //
            "cab", "rsc", "dat", "bin", "db", "mpq", "assets", "res", "pak", "resource", "package", "blob"));

    MAP.put("certificate", Arrays.asList( //
            "cer", "p12", "pem"));

    MAP.put("temp", Arrays.asList( //
            "cache", "tmp", "temp", "pid", "old", "backup", "bak"));

    MAP.put("library", Arrays.asList( //
            "dll", "ocx", "sys", "lib"));

    MAP.put("debug", Arrays.asList( //
            "pdb", "mdb", "debug"));

    // pouze kontrola unikátnosti
    var temp = new ArrayList<String>();
    for (var entry : MAP.entrySet()) {
      for (var ext : entry.getValue()) {
        if (temp.contains(ext)) {
          throw new IllegalStateException(
                  "The file extension '" + ext + "' already exists in another or the " + "same collection");
        }
        temp.add(ext);
      }
    }
  }

  /**
   * Vyřeší ikonu souboru.
   *
   * @param file Vstupní soubor.
   *
   * @return Název ikony nebo skupiny ikon.
   */
  public static String resolve(File file) {
    var fileName = file.getName();
    var ext = Utils.getFileExtension(fileName).toLowerCase();
    var icon = "";

    if (!ext.isEmpty()) {
      for (var entry : MAP.entrySet()) {
        if (entry.getValue().contains(ext)) {
          icon = String.format("g-%s ", entry.getKey());
          break;
        }
      }
      icon += "e-" + ext;
    } else {
      icon = "e-file";
    }
    LOG.debug("Resolve icon for '{}' to: '{}'", fileName, icon);
    return icon;
  }
}
