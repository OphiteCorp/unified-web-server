package oc.mimic.uwebserver.unified;

import io.netty.channel.Channel;

import java.io.File;
import java.time.Duration;
import java.time.Instant;

/**
 * Pomocné metody.
 *
 * @author mimic
 */
public final class Utils {

  /**
   * Vytáhne adresu klienta.
   *
   * @param channel Aktuální kanál (připojení klienta).
   *
   * @return IP adresa včetně přiděleného portu.
   */
  public static String getRemoteAddress(Channel channel) {
    var address = channel.remoteAddress();
    var addressStr = "<null>";

    if (address != null) {
      addressStr = address.toString().replaceAll("/", "");
    }
    return addressStr;
  }

  /**
   * Získá počet čísel v hodnotě.
   *
   * @param value Vstupní hodnota.
   *
   * @return Počet čísel.
   */
  public static int getNumberOfDigits(long value) {
    var count = 0;
    while (value != 0) {
      value /= 10;
      ++count;
    }
    return count;
  }

  /**
   * Vypočte rozdíl časů a naformátuje ho.
   *
   * @param start Počáteční čas.
   * @param end   Konečný čas.
   *
   * @return Naformátovaný čas. Obsahuje pouze to, co je relevantní.
   */
  public static String formatTime(Instant start, Instant end) {
    var d = Duration.between(start, end);
    var ms = d.toMillisPart();
    var h = d.toHoursPart();
    var m = d.toMinutesPart();
    var s = d.toSecondsPart();
    var sb = new StringBuilder();
    var addDelim = false;

    if (h > 0) {
      sb.append(String.format("%dh", h));
      addDelim = true;
    }
    if (m > 0) {
      sb.append(String.format("%s%02dm", (addDelim ? " " : ""), m));
      addDelim = true;
    }
    if (s > 0) {
      sb.append(String.format("%s%02ds", (addDelim ? " " : ""), s));
      addDelim = true;
    }
    if (ms > 0) {
      sb.append(String.format("%s%dms", (addDelim ? " " : ""), ms));
    }
    if (sb.length() == 0) {
      sb.append("immediately");
    }
    return sb.toString();
  }

  /**
   * Naformátuje velikost souboru.
   *
   * @param fileSize Velikost souboru v bytech.
   *
   * @return Naformátovaná velikost.
   */
  public static String formatFileSize(long fileSize) {
    final var unit = 1024;
    if (fileSize < unit) {
      return fileSize + " B";
    }
    var exp = (int) (Math.log(fileSize) / Math.log(unit));
    var pre = "KMGTPE".charAt(exp - 1);
    return String.format("%.2f %sB", fileSize / Math.pow(unit, exp), pre);
  }

  /**
   * Získá relativní cestu k souboru nebo adresáři.
   *
   * @param rootPath Hlavní adresář.
   * @param file     Cesta k souboru nebo adresáři v hlavním adresáři.
   *
   * @return Relativní cesta.
   */
  public static String getRelativePath(File rootPath, File file) {
    var path = file.getAbsolutePath().substring(rootPath.getAbsolutePath().length());
    return path.isBlank() ? "/" : replaceSlashes(path);
  }

  /**
   * Odebere poslední lomítko na konci textu.
   *
   * @param input Vstupní text.
   *
   * @return Upravený vstupní text.
   */
  public static String removeLastSlash(String input) {
    if (input == null) {
      return null;
    }
    if (input.endsWith("/") || input.endsWith("\\")) {
      input = input.substring(0, input.length() - 1);
    }
    return input;
  }

  /**
   * Přidá poslední lomítko na konci textu.
   *
   * @param input Vstupní text.
   *
   * @return Upravený vstupní text.
   */
  static String addLastSlash(String input) {
    if (input == null) {
      return null;
    }
    if (!input.endsWith("/") && !input.endsWith("\\")) {
      input += "/";
    }
    return input;
  }

  /**
   * Nahradí všechna zpětlá lomítka za pravá lomítka.
   *
   * @param input Vstupní text.
   *
   * @return Upravený text.
   */
  public static String replaceSlashes(String input) {
    if (input == null) {
      return null;
    }
    if (input.indexOf('\\') != -1) {
      input = input.replaceAll("\\\\", "/");
    }
    return input;
  }

  /**
   * Odebere duplicitní lomítka.
   *
   * @param input Vstupní text.
   *
   * @return Upravený text.
   */
  public static String removeDupeSlashes(String input) {
    if (input == null) {
      return null;
    }
    var array = input.toCharArray();
    var out = new StringBuilder();
    var added = false;
    var canAppend = true;

    for (var i = 0; i < array.length - 1; i++) {
      if (array[i] == '/' && array[i + 1] == '/') {
        if (!added) {
          out.append(array[i]);
        }
        added = true;
        canAppend = false;
      } else {
        added = false;
      }
      if (canAppend) {
        out.append(array[i]);
      }
      canAppend = !added;

      if (canAppend && i == array.length - 2) {
        out.append(array[i + 1]);
      }
    }
    if (out.length() == 0) {
      out.append(input);
    }
    return out.toString();
  }

  /**
   * Získá příponu souboru.
   *
   * @param fileName Vstupní název souboru.
   *
   * @return Přípona souboru.
   */
  static String getFileExtension(String fileName) {
    if (fileName == null) {
      return null;
    }
    var lastIndexOf = fileName.lastIndexOf(".");
    if (lastIndexOf == -1) {
      return "";
    }
    var ext = fileName.substring(lastIndexOf);
    if (ext.startsWith(".")) {
      ext = ext.substring(1);
    }
    return ext;
  }

  /**
   * Odebere příponu souboru z jeho názvu.
   *
   * @param fileName Název souboru.
   *
   * @return Název souboru bez přípony.
   */
  public static String removeFileExtension(String fileName) {
    if (fileName.lastIndexOf('.') != -1) {
      return fileName.substring(0, fileName.lastIndexOf('.'));
    }
    return fileName;
  }

  /**
   * Spojí řetězce pomocí delimiteru.
   *
   * @param delimiter Delimiter.
   * @param strs      Řetězce.
   *
   * @return Spojené řetězce do jednoho.
   */
  public static String join(String delimiter, String... strs) {
    var sb = new StringBuilder();

    if (strs != null) {
      for (var str : strs) {
        if (str != null && str.length() > 0) {
          sb.append(str);
          sb.append(delimiter);
        }
      }
    }
    var out = sb.toString();
    if (!out.isEmpty()) {
      out = out.substring(0, out.length() - delimiter.length());
    }
    return out;
  }
}
